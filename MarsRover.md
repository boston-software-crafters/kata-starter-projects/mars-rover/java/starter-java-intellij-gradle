# A Trip to Mars --- Fight Instructions

**Destinations**: a number of ten square mile plateaus on Mars. First up is the Meridian Plateau.

**Landing site**: south west corner of the plateau: cell coordinates (0, 0) on an x, y grid. Facing north.

**Rover capabilities**:

  1. Move command (M): move 1 mile in the current direction.

  2. Turn commands (L, R): turn the rover to the left (L) or the right (R). From a compass perspective L is counter-clockwise and R is clockwise.

  3. Quantum wrap drive: moving off the grid wraps back to the other side of the current row or column.

  A move command does not change the current direction.

  A turn command does not change the current cell coordinates.

**NASA requests**: Houston will send us sequences of M, L and/or R commands to execute, for example "LMR".

**NASA response**: Houston expects us to send back a coded response, for example "7:3:E" (x:y:direction)

**Obstacle**: an obstacle stops a move command execution.

  NASA requests may include a list of obstacles specified by cell coordinates.

  Default is an empty list.

**Obstacle response**: Prefix the response with an "O". For example: "O:5:9:W"

**Task**: Before 20:30 write a program to execute requests and return responses for NASA.

**API**: `execute(String[, List<Coordinate>]) -> String`, see examples below.

**Methodology**: TDD

**Code Coverage Requirements**: 100%

**Examples**:

    execute("MMRMMLM") -> "2:3:N"
    execute("RMMMMM", listOf("4:0") -> "O:3:0:E"
    execute("MMMMMMRMMMMMMMMLMRMMM") -> "1:7:E"

**Questions?**: Put them in the chat window.
