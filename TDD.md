# Developing a solution with TDD

Once the project is cloned and configured in **IntelliJ**, you can begin your work of solving the kata using TDD.

As you progress you can decide if and when to push your work.

We recommend that you push your final work if only to allow for working on the kata after the meetup event.

Once the developer(s) are ready to start, do the following:

1) Verify the environment is ready by clicking on the green arrow in the file `solution/src/test/java/org/bsc/katas/MainTest.java` on line 7 to run all the tests.

This test should pass verifying that the solution environment is ready.

2) Add your first real test using the pattern from the test named `when_starting_out_verify_the_environment_works`

3) Each time you write enough test code to eliminate all compilation errors, re-run all the tests in the `MainTest` class.

For any and all tests that fail, fix the production solution in the file `solution/src/main/java/org/bsc/katas/MainSolution.java`.

4) Run the code coverage tool built into **IntelliJ** by selecting the appropriate item from the run test control (green arrow).

**IntelliJ** will produce a report which provides a color coded listing for the production code. Green is good. Yellow indicates a branch not taken. Red indicates code not executed.

These yellow and red code paths reflect opportunities to add tests to improve the code coverage.  All green is the goal.

**IntelliJ** also highlights the coverage status in the editor pane gutter on a file by file basis.
