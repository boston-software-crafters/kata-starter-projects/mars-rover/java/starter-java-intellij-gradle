# Attributions

## Grid

A special thanks to Alex Ball for coming up with the grid diagram. Very, very useful and appreciated.

## Mars Rover Kata source material

The original and complete instructions can be found on the [Katalyst Site](https://katalyst.codurance.com/mars-rover) which includes a video of Sandro Mancuso solving the Mars Rover Kata. Well worth the time to peruse both.

The video is well worth watching, even before attempting the kata, but this is optional. If you do so, try to take a different approach when doing your solution.
