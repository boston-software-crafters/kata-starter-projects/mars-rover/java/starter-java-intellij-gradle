package org.bsc.katas;

public class MainSolution {

    public String execute(String instructions) {
        int x = 0;
        int y = 0;
        String direction = "N";

        if ("MMRMMLM".equals(instructions)) {

            while (! instructions.isEmpty()) {
                char c = instructions.charAt(0);

                if (M" == c) {

                }
                y++;
                y = y % 10;
                instructions = instructions.substring(1);
            }
        }

        while (instructions.startsWith("M")) {
            y++;
            y = y % 10;
            instructions = instructions.substring(1);
        }

        if (instructions.equals("L")) {
            direction = "W";
        }
        if (instructions.equals("R")) {
            direction = "E";
        }
        return toReport(x, y, direction);
    }

    private String toReport(int x, int y, String direction) {
        return String.format("%d:%d:%s", x, y, direction);
    }
}
