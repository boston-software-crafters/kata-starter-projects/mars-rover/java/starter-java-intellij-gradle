package org.bsc.katas;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainTest {
    @Test
    public void testEmptyString() {
        doTheUseCase("", "0:0:N");
    }

    @Test
    public void testSingleMove() {
        doTheUseCase("M", "0:1:N");
    }

    @Test
    public void testSingleLeftTurn() {
        doTheUseCase("L", "0:0:W");
    }

    @Test
    public void testSingleRightTurn() {
        doTheUseCase("R", "0:0:E");
    }

    @Test
    public void testTwoMoves() {
        doTheUseCase("MM", "0:2:N");
    }

    @Test
    public void testSevenMoves() {
        doTheUseCase("MMMMMMM", "0:7:N");
    }

    @Test
    public void testManyMoves() {
        String instructions = "MMMMMMMMMM";
        String expected = "0:0:N";
        doTheUseCase(instructions, expected);
    }

    @Test
    public void testTurnsAndMoves() {
        doTheUseCase("MMRMMLM", "2:3:N");
    }


    private void doTheUseCase(String instructions, String expected) {
        MainSolution sut = new MainSolution();
        String report = sut.execute(instructions);
        Assert.assertEquals(expected, report);
    }

    /*@Test
    public void testWithAnObstacle() {
        MainSolution sut = new MainSolution();
        String report = sut.execute("M, [0:1]");
        Assert.assertEquals("O:0:0:N", report);
    }   */

    // Test a single L results in 0:0:W
    // Test a single R results in 0:0:E


}
